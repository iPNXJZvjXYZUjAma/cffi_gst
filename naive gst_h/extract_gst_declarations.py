# Copyright 2020 Fehervari Tamas
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# extract_gst_declarations.py
#
# Extract function and type declarations from pre-processed GStreamer
# header files to be used in CFFI's ffi.cdef(). Why is this good?
# This way GStreamer can be directly used in Python without any
# intermediate libraries (e.g. gobject-introspection), which are not
# easy to get to work on Windows. The resulting code could probably
# be used as a starting point to use with Cython or Ctypes as well.
# On the downside, this approach only exposes the exact same programming
# interface as used in C, without the nice features of classes etc.
# Furthermore, the current implementation loses any macro definitions -
# stripped by the pre-processing step.
#
# Development and testing done only on Windows 10 x64 with Visual Studio
# compiler as pre-processor (though gcc in mingw should work as well).
#
# Usage: python <this file>.py input_file output_file
#
# Implementation:
#
# This is a hobby project done over a few afternoons, the algorithm
# is very very naive and implemented at a very simple level. I only spent
# as much time on it to make it work more-or-less correctly. Using a proper
# C parser (antlr?) would be a good idea, probably.
#
# Basically, a function declaration is defined as a block of code
# starting with the keywords 'extern' or 'static inline', and ending
# with either the end of a top-level {} block or ';'. Top-level means
# that it is not part of any outer {} or () block. The function's {} block,
# if exists, is removed and replaced by a ';' in order to convert a
# function definition to a simple declaration. The beginning keywords
# 'static inline' are discarded. Variable declarations might also begin
# with 'extern', those are mostly discarded - mostly, because the only
# check done here is to ensure that declarations have a () block, which some
# extern variables do.
#
# A type declaration begins with 'enum', 'typedef', 'struct' or 'union' and
# terminates with a top-level ';'.This means that nested type declarations
# are preserved.
#
# This script is only part of several processing steps required before the
# header can be fed to CFFI. There should be a doc describing the entire
# process somewhere in this project.
#
# Current status: proof of concept
# 1. Gst.h (meaning all basic gstreamer types and functions) probably work
#    I haven't looked at other headers (gstgl, gstapp, etc) yet.
# 2. A lot of things are simplified (this is NOT a real parser by any means)
#    but assumptions seems to stand for GST headers.
# 3. Not checked for completeness, script might miss declarations.
# 4. Macros are discarded in the pre-processing step (sadly).
# 5. Extern variables are mostly discarded. Probably a good thing?
# 6. Some inline functions are both declared and defined in the headers.
#    To avoid double declarations in the output, function names are extracted
#    with a simple algorithm (but good enough to work with gst headers),
#    and duplicates are discarded.
# 7. Output file needs manual intervention before it can be fed to CFFI.
#    For example, CFFI's pycparser fails to understand some enumerations,
#    those need to be replaced. See <gst_h_fixes.txt>.

import sys
import re
import faulthandler

# read about this recently, thought it fun to include; probably not needed
faulthandler.enable()

if len(sys.argv) != 3:
    print(f'incorrect parameter count\nusage: python {__file__} infile outfile')
    exit(-1)

infile_path = sys.argv[1]
outfile_path = sys.argv[2]

# remove empty lines & whitespace and lines beginning with #
#
# at this point lines with # are mostly pragma and other useless directives;
# macro defines have already been stripped by the pre-processor
lines = []
with open(infile_path, 'r') as inputf:
    for line in inputf:
        line = line.strip()
        if line:
            if line.startswith('#'):
                continue
            else:
                lines.append(line.strip() + '\n')

# create one contiguous block of text
text = ''.join(lines)

# type declarations to keep
# top-level declarations are preserved from keyword to finishing ;
# including any nested declaration contained within
type_decls = {'enum', 'typedef', 'struct', 'union'}

# function declarations/definitions to keep
# definitions are converted to declarations by discarding top-level {} block
# keywords are not included in the output
func_decls = {'extern', 'static inline'}

decl_first_letters = {b[0] for b in type_decls.union(func_decls)}
whitespace = {' ', '\n', '\t', '\r'}

function_names = set()

with open(outfile_path, mode='wt') as outputf:
    pos = 0
    paren_level = 0
    curlyparen_level = 0
    top_level_cp_block = [-1, -1]
    top_level_p_block = [-1, -1]
    decl_start = -1

    in_quote = False      # true inside ' strings
    in_dblquote = False   # true inside " strings
    in_type_decl = False  # true inside type declarations
    in_func_decl = False  # true inside function declarations (and extern variables)
    in_escape = False     # true inside escape sequences in strings

    # iterate over characters in text
    # could use better tools, tokenisation, regexp, etc?
    while pos < len(text):
        c_at_pos = text[pos]
        # print(c_at_pos)

        # handle string literals, keep track of escape \ characters
        # if in string, just move on until the end
        if c_at_pos == '"' and not in_quote and not in_escape:
            in_dblquote = not in_dblquote
            # print(f'{pos} - 1')

        elif c_at_pos == "'" and not in_dblquote and not in_escape:
            in_quote = not in_quote
            # print(f'{pos} - 2')

        elif in_quote or in_dblquote:
            # print(f'{pos} - 3')
            if c_at_pos == '\\':
                in_escape = not in_escape

        # keep track of () and {} blocks
        # remember top-level block boundaries
        # if within block, proceed until the end
        # this also means that any top-level declaration within a block is ignored
        # mixed nested blocks (e.g. {()}) are not considered
        elif c_at_pos == '(' and curlyparen_level == 0:
            if paren_level == 0:
                top_level_p_block = [pos, -1]
            paren_level += 1
            # print(f'{pos} - 4 - {paren_level}')

        elif c_at_pos == '{' and paren_level == 0:
            if curlyparen_level == 0:
                top_level_cp_block = [pos, -1]
            curlyparen_level += 1
            # print(f'{pos} - 5 - {curlyparen_level}')

        elif c_at_pos == ')' and curlyparen_level == 0:
            if paren_level < 1:
                print('error: orphaned )')
                print(text[pos - 40:pos] + '^^^)^^^' + text[pos + 1:pos + 41] + '\n')
            else:
                paren_level -= 1
                if paren_level == 0:
                    top_level_p_block[1] = pos
            # print(f'{pos} - 6 - {paren_level}')

        elif c_at_pos == '}' and paren_level == 0:
            if curlyparen_level < 1:
                print('error: orphaned }')
                print(text[pos - 40:pos] + '^^^}^^^' + text[pos + 1:pos + 41] + '\n')
            else:
                curlyparen_level -= 1
                # print(f'{pos} - 7 - {curlyparen_level}')
                if curlyparen_level == 0:
                    top_level_cp_block[1] = pos

                    # the end of a top-level {} block can be the end of a function definition
                    # convert it into a declaration by removing the {} block and adding ; at the end
                    if in_func_decl and decl_start < top_level_cp_block[0]:  # and paren_level == 0
                        if top_level_p_block[0] > decl_start:
                            words = re.findall('\w+', text[decl_start:top_level_p_block[0]])
                            function_name = words[-1]

                            if function_name not in function_names:
                                function_names.add(function_name)
                                # print(f'new function declaration: {function_name}')
                                decl_text = text[decl_start:top_level_cp_block[0]]
                                outputf.write('\n' + decl_text.strip() + ';\n')
                            else:
                                print(f'duplicate declaration of "{function_name}" skipped')

                                # decl_text = text[decl_start:pos + 1]
                                # print('\n' + decl_text + '\n')

                        in_func_decl = False
                        decl_start = -1

        # Declarations can only begin / end on the top level (not nested in () or {} blocks).
        # Well, this seems to be a safe assumption for GStreamer.
        elif paren_level == 0 and curlyparen_level == 0:

            # declarations end with a top-level ; (handled here) or a top-level } (already handled above)
            if c_at_pos == ';':
                if in_type_decl:
                    decl_text = text[decl_start:pos + 1]
                    outputf.write('\n' + decl_text.strip() + '\n')
                    in_type_decl = False
                    decl_start = -1
                elif in_func_decl and top_level_cp_block[1] < decl_start:

                    if top_level_p_block[0] > decl_start:
                        words = re.findall('\w+', text[decl_start:top_level_p_block[0]])
                        function_name = words[-1]

                        if function_name not in function_names:
                            function_names.add(function_name)
                            # print(f'new function declaration: {function_name}')
                            decl_text = text[decl_start:pos + 1]
                            outputf.write('\n' + decl_text.strip() + '\n')
                        else:
                            print(f'duplicate declaration of "{function_name}" skipped')

                    in_func_decl = False
                    decl_start = -1

            # look for declaration keywords in words with a suitable initial letter
            # these declaration keywords must be followed by whitespace
            elif not (in_func_decl or in_type_decl) and c_at_pos in decl_first_letters:

                for keyw in type_decls:
                    if text.startswith(keyw, pos) and text[pos + len(keyw)] in whitespace:
                        in_type_decl = True
                        decl_start = pos
                        pos += len(keyw)
                        break
                else:
                    for keyw in func_decls:
                        if text.startswith(keyw, pos) and text[pos + len(keyw)] in whitespace:
                            in_func_decl = True
                            decl_start = pos + len(keyw)
                            pos += len(keyw)
                            break

        pos += 1
