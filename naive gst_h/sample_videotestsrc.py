from _cffi_gst import lib as gst
from _cffi_gst import ffi as ffi

NULL = ffi.NULL
GST_CLOCK_TIME_NONE = ffi.cast("guint64", -1)

gst.gst_init(NULL, NULL)

ps = ffi.new("char[]", b'videotestsrc ! autovideosink')
pipeline = gst.gst_parse_launch(ps, NULL)

gst.gst_element_set_state(pipeline, gst.GST_STATE_PLAYING)

bus = gst.gst_element_get_bus(pipeline)
msg = gst.gst_bus_timed_pop_filtered(bus, GST_CLOCK_TIME_NONE, gst.GST_MESSAGE_ERROR | gst.GST_MESSAGE_EOS)
if msg is not NULL:
    print('got EOR/ERROR message, finishing up')
    gst.gst_message_unref(msg)
    gst.gst_object_unref(bus)
    gst.gst_element_set_state(pipeline, gst.GST_STATE_NULL)
    gst.gst_object_unref(pipeline)
