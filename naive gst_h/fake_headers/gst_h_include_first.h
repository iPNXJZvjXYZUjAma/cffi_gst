#ifndef INCLUDE_FIRST_H
#define INCLUDE_FIRST_H

#define __attribute__(x)
#define __extension__
#define __inline
#define __typeof__(x)
#define __pragma(x)
#define __declspec(x)
#define __cdecl
#define __static_assert_t(x)

typedef char* va_list;
typedef long long __int64;
typedef __int64 __time64_t;
typedef __time64_t time_t;

#endif