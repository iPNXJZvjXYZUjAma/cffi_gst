@echo off

rem Preprocess gst.h with VS compiler
rem currently set to process gst.h in the gstreamer directory into gst.i in the local folder
rem requires:
rem 1. Gstreamer install (base + devel), GSTREAMER_1_0_ROOT_X86_64 environment variable set
rem 2. fake_headers directory
rem 3. Visual Studio cl.exe compiler + set correct path in command
rem
rem + includes base gstreamer headers, a lot can be had in one bunch
rem + strips comments and other useless stuff
rem - discards macros
rem
rem Explanation of used parameters:
rem /wd4996 - suppress deprecation warnings; it's your responsibility not to use deprecated functions
rem /X      - ignore standard include dir; necessary extras are included in fake headers
rem /Ifake_headers  - directory holding empty fake header files required by gstreamer (e.g. stderr.h)
rem /FIgst_h_include_first.h - force include this header, which contains the few external definitions
rem                            that are really required (e.g. va_list) and discards compiler directives
rem 
rem /I*     - gstreamer include directories, needs GSTREAMER_1_0_ROOT_X86_64 environment variable
rem /EP /P  - preprocess only and write to file
rem /Tc*    - indicates C source (the header to be preprocessed)

"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\MSVC\14.25.28610\bin\HostX86\x64\cl.exe" /wd4996 /X /nologo /W3 /DNDEBUG /I%GSTREAMER_1_0_ROOT_X86_64%\include /I%GSTREAMER_1_0_ROOT_X86_64%\include\gstreamer-1.0 /I%GSTREAMER_1_0_ROOT_X86_64%\include\glib-2.0 /I%GSTREAMER_1_0_ROOT_X86_64%\lib\glib-2.0\include /Ifake_headers /FIgst_h_include_first.h /EP /P /Tc%GSTREAMER_1_0_ROOT_X86_64%\include\gstreamer-1.0\gst\gst.h /Figst.i
