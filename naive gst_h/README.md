This folder contains a working module with gst.h bindings and the files used to create it.

Works on Win 10 x64.

Prerequisites:
- MS C compiler cl.exe (Visual Studio or Build Tools)
  (actually, gcc on MinGW can do the preprocessing just as well, not sure if CFFI can use it for compiling, though)
  Path to cl.exe needs to be updated in prepro_vs.bat.
- GStreamer base + devel installed, GSTREAMER_1_0_ROOT_X86_64 environment variable set
- Python and CFFI. I used Python 3.8.1 and CFFI 1.14.0, installed with conda.

Steps:

1. <prepro_vs.bat>

Pre-processes gst.h into gst.i using Visual Studio C compiler cl.exe. Path to cl.exe needs to be updated in the file.

<fake_headers>
<fake_headers\gst_h_include_first.h>

Empty headers to provide standard libraries but to avoid actually including them, fed to cl.exe. Idea and basic implementation
based on pycparser. Gst_h_include_first.h has the few definitions gstreamer actually needs, it's force-included (/FI)
during preprocessing.

This step produces <gst.i>, a preprocessed, pretty large header file.

2. <extract_gst_declarations.py>

Further processes the preprocessed header to make it suitable for CFFI's cdef(). Extracts function and type declarations,
removes useless stuff.

Usage: python extract_gst_declarations.py input output


3. Manual fixes to eliminate all errors in cdef(): <gst_h_manual_fixes.txt>

<gst_h.cdef>

Product of above steps, can be interpreted by cdef().

+1 <cffi_gst.py>

Compile CFFI library

Usage: python cffi_gst.py gst_h.cdef

<_cffi_gst.cp38-win_amd64.pyd>

Resulting library. Perhaps this is the only file you need if you only came here for a quick way to access gstreamer.
I've compiled it with VS 2019 for GStreamer 1.17 that I built with the same compiler. There might be compatibility issues
with other GStreamer builds and on other Python and Win setups.

+2 <sample_videotestsrc.py>

Loads the CFFI module and launches a simple videotestsrc pipeline.
