import os
import sys
from pathlib import Path
from cffi import FFI

if len(sys.argv) != 2:
    print(f'incorrect parameter count\nusage: python {__file__} header_file')
    exit(-1)

# gst_dir = os.environ[[key for key in os.environ.keys() if key.lower().startswith('gstreamer')][0]]
for env in os.environ.keys():
    if env.lower().startswith('gstreamer_1_0_root'):
        gst_dir = Path(os.environ[env])
        break
else:
    print('error: environment variable for gstreamer root dir not found!')
    exit(-1)

gst_include_dirs = [b.as_posix() for b in [gst_dir / 'include',
                                           gst_dir / 'include' / 'gstreamer-1.0',
                                           gst_dir / 'include' / 'glib-2.0',
                                           gst_dir / 'lib' / 'glib-2.0' / 'include']]

gst_library_dirs = [b.as_posix() for b in [gst_dir / 'lib',
                                           gst_dir / 'lib' / 'gstreamer-1.0',
                                           gst_dir / 'lib' / 'glib-2.0']]

ffi = FFI()
with open(sys.argv[1]) as gst_h:
    ffi.cdef(gst_h.read())

# for testing, this must work:
# ffi.cdef("""
# void gst_init (int *argc, char **argv[]);
# """)

# includes gst.h for actual compilation
# VS specific pragma disables deprecation warning
ffi.set_source('_cffi_gst',
               """
               #pragma warning(disable : 4996)
               #include "gst/gst.h"
               """,
               include_dirs=gst_include_dirs,
               library_dirs=gst_library_dirs,
               libraries=["gstbase-1.0", "gstreamer-1.0", "gobject-2.0", "glib-2.0"])

if __name__ == "__main__":
    ffi.compile(verbose=True)
