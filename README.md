# CFFI Python bindings for GStreamer's gst.h on Windows

**Brief description:**
* GStreamer has Python bindings (gst-python), but they are a hassle to get to work on Windows, in my experience.
* GST libraries can be accessed directly using ctypes or CFFI and alike. CFFI has the advantage of being able to parse C code directly, but its parser has limitations.
* I've prepared a preprocessed header file that can be interpreted by CFFI's cdef(), this way a lot of GST functionality can be called from Python, just like from C.

I'm sure experienced developers can get gst-python to work, but I don't want to spend so much time with it. I also enjoy exploring other options and learning about parsers available in Python. This here project is something I threw together in a couple of afternoons to see if it would work. As it is, it's more a proof of concept but perhaps it can serve as a starting base for something more. Currently, gst.h binding seem to work well enough for a simple videotestrc test. I'm still looking into different ways to make the process more automatic (pyclibrary?). The reason for making it public is because there might be others who are looking for an alternative way to access GStreamer.

So far I've only focused on gst.h as it comprises a huge library of functions - including a ton from GLib - that alone might be enough for a complete GStreamer project.

**Pros/Cons:**
+ Dependencies simplified to GStreamer (base + devel), CFFI (https://cffi.readthedocs.io/en/latest/), and a C compiler that works with CFFI (I used MS Visual Studio Community).
- Macros are lost in the process (type cast macros, GST_BUFFER_PTS/DTS etc.)
- Untested, unproven
- No fancy Python magic (classes, like in gst-python) just the pure, raw C interface, though these could be implemented on top.

**Naive implementation (current state):**
1.  gst.h preprocessed with a C compiler (Visual Studio, but MinGW gcc also works)
2.  type and function declarations extracted with a (very) simple text processing algorithm in Python
3.  eliminate remaining problems manually (simplification of enumerations etc.)

**What's next?:**
* other headers
* ? splitting gst.h up to reduce import size (e.g. splitting off glib)
* ? more advanced parsing (pyclibrary)

**License**

My contributions in this project are licensed under the Apache 2.0 license. Some output files (e.g. gst_h.cdef and the compiled CFFI module) contain bits of or link to other libraries (GStreamer, GLib) so for those the respective licenses of those libraries apply.